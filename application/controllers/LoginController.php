<?php defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{
	//login page
	public function login()
	{
		$data['title'] = 'Login';
		$this->load->view('front-end/authorization-pages/login-view', $data);
	}

	//Register page
	public function register()
	{
		$data['title'] = 'Register';
		$this->load->view('front-end/authorization-pages/register-view', $data);
	}

	//Forgot password page
	public function forgotPassword()
	{
		$data['title'] = 'Forgot Password';
		$this->load->view('front-end/authorization-pages/forgot-password-view', $data);
	}
}
